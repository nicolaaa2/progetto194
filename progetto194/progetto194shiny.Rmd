---
title: 'Progetto 194'
author: "Franceschi Nicola"
date: "21 giugno 2016"
output: ioslides_presentation
runtime: shiny
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(quantmod)
library(ggplot2)
opt <- options("scipen" = 20)
getSymbols("T", src="yahoo")
Data <- T
d <- data.frame( Date=index(T), drop(coredata(T)))
```
## Shiny Presentation
Visualizzazione della struttura ineterna del data set Data

```{r}
str(Data)
```
## Shiny Presentation
Nomi delle colonne del data set

```{r}
names(Data)
```
## Shiny Presentation
Sommario del data set Data

```{r}
summary(Data)
```
## Shiny Presentation
Plot dell'oggetto data tramite la funzione barChart() impostando il tema su white

```{r prova}
inputPanel(
  selectInput("elements" , label = "Colonne da Visualizzare:" , choices = unique(data$name)) , selected = data$name[1]
)

data_user <- reactive({
    subset(Data, Data %in% input$elements)

    })  ##This is good


 data_user()$date <- as.Date(data_user()$date) ##Note the syntax
#input$elements
#b <- as.xts(data.frame(input$elements))

barChart(Data, theme=("white"))
```

## Shiny Presentation

This R Markdown presentation is made interactive using Shiny. The viewers of the presentation can change the assumptions underlying what's presented and see the results immediately. 

To learn more, see [Interactive Documents](http://rmarkdown.rstudio.com/authoring_shiny.html).

## Interactive Plot

```{r eruptions}
inputPanel(
  selectInput("n_breaks", label = "Number of bins:",
              choices = c(10, 20, 35, 50), selected = 20),
  
  sliderInput("bw_adjust", label = "Bandwidth adjustment:",
              min = 0.2, max = 2, value = 1, step = 0.2)
)

renderPlot({
  hist(faithful$eruptions, probability = TRUE, breaks = as.numeric(input$n_breaks),
       xlab = "Duration (minutes)", main = "Geyser eruption duration")
  
  dens <- density(faithful$eruptions, adjust = input$bw_adjust)
  lines(dens, col = "blue")
})
```

Cazzi

```{r cars}
summary(cars)
```


