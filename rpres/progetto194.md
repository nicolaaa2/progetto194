Progetto194
========================================================
author: Franceschi Nicola
date: 21 Giugno 2016
autosize: true
width: 1366
height: 768
css: custom.css


<style>
  .first {
    color: black; background: transparent;
    position: absolute; top: 8%; left: 50%;
    width:50%;
  }
  .logo {
    color: black; background: transparent;
    position: absolute; top: -8%; left: 97%;
    width:20%;
  }
  .last {
    color: black; background: transparent;
    position: absolute; top: -5%; left: 5%;
    width:100%;
  }
</style>

<div class="first">
<img src="LogoUniFe.png"></img>
</div>



Visualizzazione della struttura ineterna del data set Data
========================================================
incremental: true
<div class="logo">
<img src="LogoUniFe.png"></img>
</div>


```r
str(Data)
```

```
An 'xts' object on 2007-01-03/2016-06-22 containing:
  Data: num [1:2385, 1:6] 35.7 35 34.4 33.4 33.8 ...
 - attr(*, "dimnames")=List of 2
  ..$ : NULL
  ..$ : chr [1:6] "T.Open" "T.High" "T.Low" "T.Close" ...
  Indexed by objects of class: [Date] TZ: UTC
  xts Attributes:  
List of 2
 $ src    : chr "yahoo"
 $ updated: POSIXct[1:1], format: "2016-06-23 02:42:50"
```

Nomi delle colonne del data set
========================================================
incremental: true
<div class="logo">
<img src="LogoUniFe.png"></img>
</div>


```r
names(Data)
```

```
[1] "T.Open"     "T.High"     "T.Low"      "T.Close"    "T.Volume"  
[6] "T.Adjusted"
```

Sommario del data set Data
========================================================
incremental: true
<div class="logo">
<img src="LogoUniFe.png"></img>
</div>


```r
summary(Data)
```

```
     Index                T.Open          T.High          T.Low      
 Min.   :2007-01-03   Min.   :22.09   Min.   :22.78   Min.   :20.90  
 1st Qu.:2009-05-15   1st Qu.:28.55   1st Qu.:28.82   1st Qu.:28.26  
 Median :2011-09-26   Median :33.70   Median :33.94   Median :33.45  
 Mean   :2011-09-27   Mean   :32.70   Mean   :32.97   Mean   :32.40  
 3rd Qu.:2014-02-10   3rd Qu.:35.86   3rd Qu.:36.14   3rd Qu.:35.57  
 Max.   :2016-06-22   Max.   :42.77   Max.   :42.97   Max.   :42.71  
    T.Close         T.Volume           T.Adjusted   
 Min.   :21.72   Min.   :  6108400   Min.   :14.48  
 1st Qu.:28.51   1st Qu.: 20142600   1st Qu.:21.08  
 Median :33.67   Median : 24538000   Median :24.82  
 Mean   :32.69   Mean   : 27096104   Mean   :25.79  
 3rd Qu.:35.84   3rd Qu.: 30938800   3rd Qu.:30.97  
 Max.   :42.83   Max.   :130641500   Max.   :41.30  
```

Plot dell'oggetto data tramite la funzione barChart() impostando il tema white
========================================================
incremental: true
transition: fade
<div class="logo">
<img src="LogoUniFe.png"></img>
</div>


```r
barChart(Data , theme=("white"))
```

========================================================
transition: fade
<div class="logo">
<img src="LogoUniFe.png"></img>
</div>


<img src="progetto194-figure/unnamed-chunk-5-1.png" title="plot of chunk unnamed-chunk-5" alt="plot of chunk unnamed-chunk-5" width="1920px" />

Riproduzione parte inferiore Del grafico barChart tramite la funzione plot()
========================================================
incremental: true
transition: fade
<div class="logo">
<img src="LogoUniFe.png"></img>
</div>


```r
par(las=1 , mar=c(5,9,2,1))
plot(T$T.Volume,type="l", main="AT&T Inc." ,ann=F)
mtext("Volume", side=2, line = 6)
mtext("Date" , side = 1 , line = 3)
title("AT&T Inc.")
```

========================================================
transition: fade
<div class="logo">
<img src="LogoUniFe.png"></img>
</div>

<img src="progetto194-figure/unnamed-chunk-7-1.png" title="plot of chunk unnamed-chunk-7" alt="plot of chunk unnamed-chunk-7" width="1920px" />

Grafico a piacimento
========================================================
incremental: true
transition: fade
<div class="logo">
<img src="LogoUniFe.png"></img>
</div>


```r
chartSeries(T  ,subset='last 5 months',name ='AT&T Inc.',theme='white',TA='addBBands()',up.col='white',dn.col='blue')
```

========================================================
transition: fade
<div class="logo">
<img src="LogoUniFe.png"></img>
</div>

<img src="progetto194-figure/unnamed-chunk-9-1.png" title="plot of chunk unnamed-chunk-9" alt="plot of chunk unnamed-chunk-9" width="1920px" />

========================================================

<div class="last">
<img src="L1.JPG"></img>
</div>

